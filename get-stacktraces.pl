#! /usr/bin/env perl
# Copyright (C) 2020 Thomas Jahns <jahns@dkrz.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
use strict;
use warnings;

use Cwd ();
use File::Temp ();
use Pod::Usage;
use Getopt::Long ();

sub usage($;@);
sub parseOptions;

my ($debug, $verbose, $attach, $access_mode) = (0, 0, 0, undef);
my ($gdb, $lsof) = ('/usr/bin/gdb', '/usr/bin/lsof');
if (-x '/usr/sbin/lsof') {
  $lsof = '/usr/sbin/lsof';
}

parseOptions();

my ($jobstate, $numhosts, $jobhosts);

usage(1) unless @ARGV;

if ($attach) {
  my (@executables) = @ARGV;
  my %pids;
  my $hostname;
  if ($debug > 2) {
    $hostname = qx(hostname);
    chomp($hostname);
  }
  for my $executable (@executables) {
    my @pids = qx{$lsof -t $executable};
    chomp(@pids);
    $pids{$executable} = [ @pids ] if (@pids);
    print $hostname, join(':', @pids), "\n"
        if ($debug > 2);
  }
  for my $executable (@executables) {
    next unless exists($pids{$executable});
    my ($gdb_script_fh, $gdb_script_filename)
        = File::Temp::tempfile('get-stacktrace-gdb-script-XXXXXXXX',
                               'TMPDIR' => 1, 'UNLINK' => 1);
    for my $pid (@{$pids{$executable}}) {
      print($gdb_script_fh 'attach ', $pid, "\n",
            'bt', "\n",
            'detach', "\n");
    }
    # print($gdb_script_fh 'quit', "\n");
    close($gdb_script_fh)
        or die();
    system('/bin/cat', $gdb_script_filename)
        && die() if ($debug > 1);
    my $gdb_output = qx($gdb -batch -x $gdb_script_filename)
        or die();
    print $gdb_output;
  }
}
else {
  my ($jobid, @executables) = @ARGV;
  my @squeue_out = split /\n/m, qx{squeue --noheader --jobs=$jobid -o '\%t \%D \%R'};
  usage(1) if ($?);
  print STDERR join("\n", @squeue_out), "\n" if ($debug > 2);
  for my $executable (@executables) {
    $executable = Cwd::realpath($executable);
    if (! -x $executable) {
      usage(1, 'error: ', $executable, " is not an executable program\n");
    }
  }
  my @cmd;
  if ($access_mode eq 'srun_overlap') {
    @cmd = ('srun', '--label', '--overlap', '--ntasks-per-node=1',
            '--jobid='.$jobid,
            Cwd::realpath(__FILE__),
            ('--debug') x $debug,
            '--gdb', $gdb, '--attach', @executables);
  } elsif ($access_mode eq 'clush') {
    my $jobhosts;
    foreach my $job_part (@squeue_out) {
      my ($jobstate, $numhosts, $job_part_hosts)
          = split(/\s+/, $job_part);
      if ($jobstate eq 'R')
      {
        $jobhosts .= ',' if (defined($jobhosts));
        $jobhosts .= $job_part_hosts;
      }
      else
      {
        die('Cannot attach to job not running (current state ', $jobstate, ")!\n");
      }
    }
    @cmd = ('clush', '-w', $jobhosts,
            '--options=-o StrictHostKeyChecking=accept-new',
            Cwd::realpath(__FILE__),
            ('--debug') x $debug,
            '--gdb', $gdb, '--attach', @executables);
  }
  else {
    die('Unexpected access mode: ', $access_mode, "\n");
  }
  print STDERR join(' ', @cmd), "\n" if ($debug > 1 or $verbose);
  system(@cmd);
}


sub usage($;@)
{
  my ($exitcode, @err_message) = @_;
  print STDERR "Invoke as\n\n $0 SLURM_JOB_ID EXECUTABLE [EXECUTABLE...]\n",
      @err_message?@err_message:();
  exit($_[0]);
}

sub parseOptions
{
  local $_;
  my ($help, $usage)=(0, 0);
  my $optionParser = new Getopt::Long::Parser;
  configureOptionParser($optionParser, 'no_auto_abbrev', 'no_ignore_case');
  my $result
      = $optionParser->getoptions('debug+' => \$debug,
                                  'attach' => \$attach,
                                  'access-mode=s' => \$access_mode,
                                  'lsof=s' => \$lsof,
                                  'gdb=s' => \$gdb,
                                  'help|?!' => \$help,
                                  'usage!' => \$usage,
                                  'verbose+' => \$verbose,
                                 );
  if($help or $usage)
  {
    pod2usage( {
                '-msg' => "",
                '-exitval' => 0,
                '-verbose' => 1
               });
  }
  if (!defined($access_mode)) {
    my $srun_help = qx{srun --help};
    $access_mode = ($srun_help =~ m{\s+--overlap\s+})
        ? 'srun_overlap' : 'clush';
  }
}

# evil hack to work with older versions of Getopt::Long
sub configureOptionParser
{
   my ($optionParser, @options) = (@_);
   eval {
      $optionParser->configure(@options);
   };
   if ($@)
   {
      my $save = Getopt::Long::Configure ($optionParser->{settings}, @options);
      $optionParser->{settings} = Getopt::Long::Configure($save);
   }
}



__END__

=head1 NAME

get-stacktraces - aquire stack traces from binaries running in SLURM job

=head1 SYNOPSIS

get-stacktraces [options] I<SLURM_JOB_ID> F<PATH> [F<PATH>...]

Execute clush to every node in I<JOBID> and attach to every binary
matching any F<PATH> specified on the command-line.

=head1 OPTIONS

=over 8

=item B<--access-mode=[srun|clush]>

Sets the method to access the nodes, the default is to use srun with
option --overlap but in case srun is too old, clush can be used
alternatively.

=item B<--gdb=>F<PATH>

Sets the gdb binary to be used, defaults to /usr/bin/gdb.

=item B<--lsof=>F<PATH>

Sets the lsof binary to be used, defaults to /usr/bin/lsof or
/usr/sbin/lsof (if the latter exists).

=item B<--help>, B<--usage>

Print usage information.

=item B<--debug>

Produces debugging output.

=item B<--verbose>

Print verbose progress information.

=back

=cut

# Local Variables:
# mode: cperl
# End:
