#! /bin/bash
jobid=$(sbatch --parsable -n 4 -p "${SHARED_PARTITION-shared}" <<EOF
#! /bin/bash
exec srun sleep 100
EOF
     )
sleep 20
jobstate=$(squeue --noheader --jobs="$jobid" -o '%t')
while [[ "${jobstate}" != 'R' ]]; do
  case "${jobstate}" in
    ('CA'|'CG'|'CD'|'DL'|'NF'|'OOM'|'PR')
      printf '%s\n' 'error: failed to start test-job!' >&2
      exit 1
      ;;
  esac
  sleep 0.5
  jobstate=$(squeue --noheader --jobs="$jobid" -o '%t')
done
../get-stacktraces.pl "${jobid}" "$(command -v sleep)"
scancel "$jobid"
rm "slurm-${jobid}.out"
