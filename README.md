# NAME

get-stacktraces - aquire stack traces from binaries running in SLURM job

# SYNOPSIS

get-stacktraces \[options\] _SLURM\_JOB\_ID_ `PATH` \[`PATH`...\]

Execute clush to every node in _JOBID_ and attach to every binary
matching any `PATH` specified on the command-line.

# OPTIONS

- **--access-mode=\[srun|clush\]**

    Sets the method to access the nodes, the default is to use srun with
    option --overlap but in case srun is too old, clush can be used
    alternatively.

- **--gdb=**`PATH`

    Sets the gdb binary to be used, defaults to /usr/bin/gdb.

- **--lsof=**`PATH`

    Sets the lsof binary to be used, defaults to /usr/bin/lsof or
    /usr/sbin/lsof (if the latter exists).

- **--help**, **--usage**

    Print usage information.

- **--debug**

    Produces debugging output.

- **--verbose**

    Print verbose progress information.
